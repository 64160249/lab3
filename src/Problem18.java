import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        int type = kb.nextInt();
        int num;
        switch (type) {
            case 1:
                System.out.print("Please input number: ");
                num = kb.nextInt();
                for (int i = 0; i < num; i++) {
                    for (int j = 0; j < num; j++) {

                        if (i == j) {

                            System.out.print("*");

                        } else if (i >= j) {
                            System.out.print("*");
                        }
                    }
                    System.out.println();
                }
                break;

            case 2:
                System.out.print("Please input number: ");
                num = kb.nextInt();
                for (int i = 0; i < num; i++) {
                    for (int j = 0; j < num; j++) {

                        if (i == j) {

                            System.out.print("*");

                        } else if (i <= j) {
                            System.out.print("*");
                        }
                    }
                    System.out.println();
                }
                break;
            case 3:
                System.out.print("Please input number: ");
                num = kb.nextInt();
                for (int i = 0; i < num; i++) {
                    for (int j = 0; j < num; j++) {

                        if (i <= j) {

                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
                break;

            case 4:
                System.out.print("Please input number: ");
                num = kb.nextInt();
                for (int i = 0; i < num; i++) {
                    for (int j = num; j >= 0; j--) {

                        if (i >= j) {

                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
                break;
            case 5:
                System.out.print("Bye bye!!!");
                break;
            default:
                System.out.println("Error: Please input number between 1-5");

        }

    }

}
